#include <complex>
#include <cmath>
#include <tuple>
#include <GL/glut.h>

namespace stoun
{
  constexpr auto min = std::complex< double >{-1.5, -1.0};
  constexpr auto max = std::complex< double >{0.7, 1.0};
  //constexpr auto min = std::complex< double >{-1.1560, 0.2777};
  //constexpr auto max = std::complex< double >{-1.1535, 0.2791};

  constexpr auto offset = std::complex< double >{-1.5, -1.0};
  constexpr auto scale = 0.0043;

  constexpr auto width = 2*512u;
  constexpr auto height = 2*512u;

  constexpr auto max_iterations = 255u;
}

namespace stoun
{
  std::complex< double > to_complex(const std::complex< unsigned int > rhs)
  {
    auto x_range = max.real() - min.real();
    auto y_range = max.imag() - min.imag();
    return {min.real() + rhs.real() * (x_range / width), min.imag() + rhs.imag() * (y_range / height)};
  }

  std::complex< double > ya_to_complex(const std::complex< unsigned int > rhs)
  {
    return {offset.real() + rhs.real() * scale, offset.imag() + rhs.imag() * scale};
  }

  unsigned int mandelbrot(const std::complex< double > rhs)
  {
    auto i = 0u;
    auto z = std::complex< double >{0.0d, 0.0d};
    while(i < max_iterations && std::abs(z) < 2)
    {
      z = std::pow(z, 2) + rhs;
      ++i;
    }
    return i;
  }

  std::tuple< float, float, float > to_color(const unsigned int rhs)
  {
    auto rhs_d = static_cast< double >(rhs % 256);
    return {rhs_d / 256, rhs_d / 256, rhs_d / 256};
  }
}

template< size_t W, size_t H >
void display()
{
  using namespace stoun;
  glClear(GL_COLOR_BUFFER_BIT);
  glBegin(GL_POINTS);
  for(auto x = 0u; x < W; ++x)
  {
    for(auto y = 0u; y < H; ++y)
    {
      auto argum = ya_to_complex({x, y});
      auto iters = mandelbrot(argum);
      auto color = to_color(iters);
      glColor3f(std::get< 0 >(color), std::get< 1 >(color), std::get< 2 >(color));
      glVertex2f(x, y);
    }
  }
  glEnd();
  glutSwapBuffers();
}

int main(int argc, char ** argv)
{
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  glutInitWindowSize(stoun::width, stoun::height);
  glutInitWindowPosition(20, 810);
  glutCreateWindow("Mandelbrot");
  glClearColor(0, 0, 0, 1.0);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, stoun::width, stoun::height, 0, -1, 1);
  glutDisplayFunc(display< stoun::width, stoun::height >);
  glutMainLoop();
  return 0;
}
